from django.db import models

# Create your models here.


class Deputy(models.Model):
    name = models.CharField(max_length=100, null=True)
    url = models.URLField(max_length=300, null=True)
    photo = models.TextField(null=True)
    photo_url = models.URLField(max_length=300, null=True)
    discriptional_info = models.TextField(null=True)
    general_info = models.TextField(null=True)
    work_hard = models.TextField(null=True)
    level_1_info = models.TextField(null=True)
    simple_info = models.TextField(null=True)
    toptitle_info = models.TextField(null=True)
    deputats_assistants = models.TextField(null=True)
    contacts_info = models.TextField(null=True)
    deputat_eregistr = models.TextField(null=True)
    deputat_manual_registr = models.TextField(null=True)
    deputat_frakcia = models.TextField(null=True)
    deputat_post = models.TextField(null=True)

    def __str__(self):
        return "%s ID: %s" % (self.name, self.id)

    class Meta:
        abstract = True
        db_table = "deputy"



class Deputy8(Deputy):
    class Meta:
        db_table = "deputy_8"
        ordering = ['id']
        verbose_name_plural = "Депутати 8ого скликання"

class Deputy9(Deputy):
    deputat_frakcia = None
    class Meta:
        db_table = "deputy_9"
        ordering = ['id']
        verbose_name_plural = "Депутати 9ого скликання"
