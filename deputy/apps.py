from django.apps import AppConfig


class DeputyConfig(AppConfig):
    name = 'deputy'
