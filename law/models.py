from django.db import models

# Create your models here.


class Law(models.Model):
    name = models.CharField(null=True, max_length=10000)
    url = models.CharField(null=True, max_length=10000)
    date = models.DateTimeField(null=True)
    zn_attr = models.CharField(null=True,  max_length=1000)
    detail_items = models.CharField(null=True, max_length=50000)
    flow_tab = models.CharField(null=True, max_length=10000)
    komited_tab = models.CharField(null=True, max_length=10000)
    linked_tab = models.CharField(null=True,  max_length=1000)
    alternative_tab = models.CharField(null=True, max_length=1000)
    # vote_tab =  models.CharField(null = True, max_length=100000)
    # vote_result_tab =  models.CharField(null = True, max_length=100000)

    class Meta:
        abstract = True
        db_table = "law"
        ordering = ['id']

class Law9(Law):
    class Meta:
        db_table = "law_9"
        ordering = ['id']