from django.db import models
from deputy.models import Deputy8, Deputy, Deputy9
# Create your models here.


class Mregistration(models.Model):
    mdeputy = models.ForeignKey(
        Deputy8,
        related_name="mdeputy",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    # '17.01.2018'
    date = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True
    )
    # Ранкове засідання, Вечірнє засідання
    mregistration_name = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    mregistration_url = models.URLField(
        max_length=500,
        null=True,
        blank=True
    )

    # '7 сесія', '6 сесія'
    nomses = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    # 'Зареєстрований' , 'У відрядженні', 'Незареєстрований'
    mregistration = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    class Meta:
        db_table = "mregistration"
        abstract = True

class Mregistration9(Mregistration):
    mdeputy = models.ForeignKey(
        Deputy9,
        related_name="mdeputy_id",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    class Meta:
        db_table = "mregistration_9"