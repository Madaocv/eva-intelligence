from django.db import models
from deputy.models import Deputy8, Deputy, Deputy9
# Create your models here.


class Eregistration(models.Model):
    edeputy = models.ForeignKey(
        Deputy,
        related_name="edeputy",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    # '12.07.2019 10:11:56'
    timestamp = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True
    )
    # Ранкова реєстрація, Вечірня реєстрація
    eregistration_name = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    # 'Присутній' , 'Відсутній'
    eregistration = models.CharField(
        max_length=50,
        null=True,
        blank=True
    )
    # '* До Голови Верховної Ради України надійшла заява .... проханням вважати результатом її електронної реєстрації - "Відсутня".'
    eregistration_change = models.CharField(null=True, blank=True, max_length=1000)
    # Всього- 385
    eregistration_all = models.PositiveSmallIntegerField(null=True, blank=True)
    # 'http://w1.c1.rada.gov.ua/pls/radan_gs09/ns_reg?g_id=29069'
    eregistration_url = models.URLField(
        max_length=500,
        null=True,
        blank=True
    )

    class Meta:
        db_table = "eregistration"
        abstract = True

class Eregistration9(Eregistration):
    edeputy = models.ForeignKey(
        Deputy9,
        related_name="edeputy_id",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    class Meta:
        db_table = "eregistration_9"