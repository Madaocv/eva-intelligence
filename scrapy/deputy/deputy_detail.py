# -*- encoding: utf-8 -*-
import bs4
from bs4 import BeautifulSoup
import requests
from deputy_model_peewee import Deputy_9 as DeputatItem
import argparse
import pandas as pd
import time
from progress_bar_x import printProgressBar

def parse_deputat_detail(detail_page_url):
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
        }
    url = detail_page_url
    resp = requests.get(url, headers=headers)
    soup = BeautifulSoup(resp.content, 'lxml', from_encoding='utf-8')
    deputat = {}
    simple_info_table = soup.find("div", {"class":"information_block_ins"}).find("table", {"class":"simple_info"}).find_all('td')[-1]

    # General info
    mp_general_info = soup.find("div",{"class":"mp-general-info"})
    ful_list_dd = []
    for idx, item in enumerate(mp_general_info.find_all('dd')):
        if len(item.text)>0:
            ful_list_dd.append(item.text)
    ful_list_dt = []
    for idx, item in enumerate(mp_general_info.find_all('dt')):
        if len(item.text)>0:
            ful_list_dt.append(item.text)
    tuplelist = list(zip(ful_list_dt, ful_list_dd))
    detail_items = {a: b for a, b in tuplelist}
    deputat["general_info"] = detail_items

    # Work hard info
    try:
        simple = []
        simple_1 = simple_info_table.br.next_sibling
        simple_2 = simple_1.next_sibling.next_sibling.next_sibling.next_sibling
        simple_3 = simple_info_table.find_all('br')[-1].previous_sibling.previous_sibling.previous_sibling.previous_sibling
        simple_4 = simple_info_table.find_all('br')[-1].previous_sibling.previous_sibling
        simple.append(simple_1.strip())
        simple.append(simple_2.strip())
        simple.append(simple_3.strip())
        simple.append(simple_4.strip())
    except Exception:
        simple = None
    deputat['work_hard'] = simple

    # Level 1 info
    level_1_info = []
    level_1_info_list =soup.find("ul",{"class":"level1"}).find_all('li')
    for a in level_1_info_list:
        try:
            level_1_info.append({a.text.strip():a.a.get('href')})
        except:
            level_1_info.append({a.text.strip():None})
    if len(level_1_info) == 0:
        level_1_info = None
    deputat["level_1_info"] = level_1_info

    # Simple info
    s_true_info_list = []
    s_true_info = soup.find_all("table", {"class": "simple_info"})[-1].find_all('td')
    s_true_info_list.append({s_true_info[0].text: s_true_info[1].text})
    s_true_info_list.append({s_true_info[2].text: s_true_info[3].text})
    if len(s_true_info_list) == 0:
        s_true_info_list = None
    deputat["simple_info"] = s_true_info_list

    # TopTitle
    top_title_list = []
    top_title = soup.find("div", {"class": "topTitle"}).find_all('a')
    for obj in top_title:
        try:
            top_title_list.append({obj.text.strip(): obj.get('href')})
        except:
            top_title_list.append({obj.text.strip(): None})
    if len(top_title_list) == 0:
        top_title_list = None
    deputat["toptitle_info"] = top_title_list

    # deputats_assistants
    try:
        assistent_types = soup.find_all('span', attrs={'style': 'font-style:italic; font-weight: bold;color:#667;'})
        assistents_dict = {assistent_type.text.strip():([' '.join(assistent.split()) for assistent in assistent_type.next_sibling.split(',')]) for assistent_type in assistent_types}
        deputat["deputats_assistants"] = assistents_dict
    except Exception:
        deputat["deputats_assistants"] = None
    try:
        contacts_info = list(soup.find("div",{"class":"information_block_ins"}).find_all('div', attrs={'style': 'clear: both; display: block; padding: 10px 0px 10px 0px;'}))
        contacts_info_list = [contact.a['href'] for contact in list(contacts_info[-1].next_siblings) if len(contact)!=0 and not isinstance(contact, bs4.element.NavigableString)]
        deputat["contacts_info"] = contacts_info_list
    except Exception:
        deputat["contacts_info"] = None
    return deputat


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Initialization of parsing scripts')
    parser.add_argument('--saveto', '-s', choices=['db', 'csv'], help='Save results to db/csv. Usage: deputy_detail.py --saveto csv') 
    args = vars(parser.parse_args())
    # Парсинг дедальної сторінки кожного депутата відбувається шляхом почергового переходу по полях DeputatItem.url
    if args['saveto']=='db':
        # У випадку коли збередення даних з детальної сторінки очмкується в базу даних 
        # ми отримаємо оновлені поля в таблиці deputy_9 а саме: general_info, work_hard, level_1_info, simple_info, toptitle_info, deputats_assistants, contacts_info
        # В репозиторію в папці doc/2_detail.png намальовані поля які оновить цей скрипт
        l = len(DeputatItem.select().order_by(DeputatItem.id))
        for i, a in enumerate(DeputatItem.select().order_by(DeputatItem.id)):
            # for a in DeputatItem.select().where(DeputatItem.id < 3).order_by(DeputatItem.id):
            deputat = parse_deputat_detail(a.url)
            try:
                res = (DeputatItem
                .update({
                    DeputatItem.general_info: deputat["general_info"],
                    DeputatItem.work_hard: deputat['work_hard'],
                    DeputatItem.level_1_info: deputat["level_1_info"],
                    DeputatItem.simple_info: deputat["simple_info"],
                    DeputatItem.toptitle_info: deputat["toptitle_info"],
                    DeputatItem.deputats_assistants: deputat["deputats_assistants"],
                    DeputatItem.contacts_info: deputat["contacts_info"],
                    })
                .where(DeputatItem.id == a.id)
                .execute())
                # print('SAVED :', a.id)
            except Exception as e:
                print(e)
                print('DO NOT SAVED :', a.id)
            printProgressBar(i + 1, l, prefix = 'Progress:', suffix = f'Complete deputy id: {a.id}', length = 50)
    elif args['saveto']=='csv':
        # Аналогічно ті ж самі поля добавляться коли результат скрапінгу зберігається в csv
        data = pd.read_csv("export_dataframe.csv") 
        list_to_csv = []
        l = len(data['url'])
        for i, a in enumerate(data['url']):
            list_to_csv.append(parse_deputat_detail(a))
            printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        data_new = pd.DataFrame(list_to_csv)
        data_frame = data.join(data_new)
        data_frame.to_csv('export_dataframe_with_details.csv', index = None, header=True)
