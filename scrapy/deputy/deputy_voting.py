# -*- encoding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import datetime
# import base64
# from pprint import pprint
import ast
import time
# import time
from urllib.parse import urljoin
import re
from deputy_model_peewee import Deputy_9 as DeputatItem
from vote_model_peewee import GeneralVote_Django
from vote_model_peewee import Vote_Django
from vote_model_peewee import VoteMeta_Django
import datetime
from progress_bar_x import printProgressBar


def deputats_personal_voting(kod, deputat_id):

    url_voting = 'http://w1.c1.rada.gov.ua'
    # kod = "238"
    kod = kod
    url_post_personal_voting = 'http://w1.c1.rada.gov.ua/pls/radan_gs09/ns_dep_gol_list'
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
        }
    params = {
        # "startDate": "27.11.2014",
        # "endDate": "18.07.2019",
        "startDate": "29.08.2019",
        "endDate": datetime.datetime.now().date().strftime('%d.%m.%Y'),
        "kod": kod,
        "nom_str": "0"
        }
    resp = requests.post(url_post_personal_voting, params=params, headers=headers)
    # print("response status code:", resp.status_code)
    soup = BeautifulSoup(resp.content, 'lxml', from_encoding='utf-8')
    # voting_li = soup.find("ul",{"class":"pd"}).find_all('li')
    # voting_li = soup.find("ul", {"class": "pd"}).find_all('li')
    wg = soup.find("ul", {"class": "pd"}).find_all("div", {"class": "strdate"})
    v_global = []
    for idx, a in enumerate(wg):
        try:
            x= a.parent.find_next_siblings("li")
            x1 = x[:x.index(wg[idx+1].parent)]
            for t in x1:
                vot = {}
                vot['exnomer'] = t.find("div", {"class": "exnomer"}).text.strip()
                vote_utl = t.find("div", {"class": "zname"}).a.get('href')
                vote_url = urljoin(url_voting, vote_utl)
                vot['vote_url'] = vote_url
                vot['text'] = t.find("div", {"class": "zname"}).a.text.strip()
                vot['personal_vote'] = t.find("div", {"class": "zrez"}).text.strip()
                vot['time'] = a.text.strip() + ' ' + t.find("ul", {"class": "npd"}).b.text.split(' - ')[1]
                try:
                    change_vote = t.find("div", {"class": "zrez"}).font.font.text.strip()
                except Exception:
                    change_vote = None
                # # chaeck if deputat change vote
                if change_vote:
                    vot['goal'] = t.find("ul", {"class": "npd"}).text.strip().split('\n')[1]
                    vot['result'] = t.find("ul", {"class": "npd"}).text.strip().split('\n')[2].strip()
                    vot['change_vote'] = ''.join(t.find("ul", {"class": "npd"}).text.strip().split('*')[1].split('\n'))
                else:
                    vot['goal'] = t.find("ul", {"class": "npd"}).text.strip().split('\n')[-2]
                    vot['result'] = t.find("ul", {"class": "npd"}).text.strip().split('\n')[-1].strip()
                    vot['change_vote'] = None
                v_global.append(vot)
        except Exception as e:
            xe = a.parent.find_next_siblings("li")
            for t in xe:
                vot = {}
                vot['exnomer'] = t.find("div", {"class": "exnomer"}).text.strip()
                vote_utl = t.find("div", {"class": "zname"}).a.get('href')
                vote_url = urljoin(url_voting, vote_utl)
                vot['vote_url'] = vote_url
                vot['text'] = t.find("div", {"class": "zname"}).a.text.strip()
                vot['personal_vote'] = t.find("div", {"class": "zrez"}).text.strip()
                vot['time'] = a.text.strip() + ' ' + t.find("ul", {"class": "npd"}).b.text.split(' - ')[1]
                try:
                    change_vote = t.find("div", {"class": "zrez"}).font.font.text.strip()
                except Exception:
                    change_vote = None
                if change_vote:
                    vot['goal'] = t.find("ul", {"class": "npd"}).text.strip().split('\n')[1]
                    vot['result'] = t.find("ul", {"class": "npd"}).text.strip().split('\n')[2].strip()
                    vot['change_vote'] = ''.join(t.find("ul", {"class": "npd"}).text.strip().split('*')[1].split('\n'))
                else:
                    vot['goal'] = t.find("ul", {"class": "npd"}).text.strip().split('\n')[-2]
                    vot['result'] = t.find("ul", {"class": "npd"}).text.strip().split('\n')[-1].strip()
                    vot['change_vote'] = None
                v_global.append(vot)
    for idx, object_vote in enumerate(v_global):
        timestamp = datetime.datetime.strptime(object_vote['time'], '%d.%m.%Y %H:%M:%S')
        # GeneralVote_Django
        query_generalvote = GeneralVote_Django.select().where(
            GeneralVote_Django.name == object_vote['text'],
            GeneralVote_Django.timestamp == timestamp
        )
        if query_generalvote.exists():
            pass
            # print('GeneralVote_Django exist')
        else:
            GeneralVote_Django.create(
                name=object_vote['text'],
                timestamp=timestamp,
                count_yes=int(re.search('За- (.*) Проти', object_vote['goal'].strip()).group(1)),
                count_not=int(re.search('Проти- (.*) Утримались', object_vote['goal'].strip()).group(1)),
                count_hold_out=int(re.search('Утримались- (.*) Не голосували', object_vote['goal'].strip()).group(1)),
                count_not_vote=int(re.search('Не голосували- (.*) Всього', object_vote['goal'].strip()).group(1)),
                count_all=int(re.search('Всього- (.*)', object_vote['goal'].strip()).group(1)),
                resolution=object_vote['result'],
            )
        query_generalvote_get = GeneralVote_Django.get(
            GeneralVote_Django.name == object_vote['text'],
            GeneralVote_Django.timestamp == timestamp
        )
        # # Vote_Django
        query_vote = Vote_Django.select().where(Vote_Django.url == object_vote['vote_url'])
        if query_vote.exists():
            pass
            # print('Vote_Django exist')
        else:
            Vote_Django.create(
                url=object_vote['vote_url'],
                # law = ,
                general_id=query_generalvote_get.id,
            )
        query_vote_get = Vote_Django.get(Vote_Django.url == object_vote['vote_url'])
        query_vote_e = VoteMeta_Django.select().where((VoteMeta_Django.deputy_id == deputat_id) & (VoteMeta_Django.voteforeign_id == query_vote_get))
        if query_vote_e.exists():
            # print('VoteMeta_Django exist')
            pass
        else:
            VoteMeta_Django.create(
                deputy_id=deputat_id,
                vote=object_vote['personal_vote'],
                vote_change=object_vote['change_vote'],
                voteforeign_id=query_vote_get,
            )
if __name__ == '__main__':
    # print('Drop')
    # GeneralVote_Django.drop_table()
    # Vote_Django.drop_table()
    # VoteMeta_Django.drop_table()
    # print('Creating')
    # GeneralVote_Django.create_table()
    # Vote_Django.create_table()
    # VoteMeta_Django.create_table()
    # print(VoteMeta_Django._meta.fields)

    # Main loop

    exeption_id_list = []
    l = len(DeputatItem.select())
    # print('XXX',l)
    for i, a in enumerate(DeputatItem.select().order_by(DeputatItem.id)):
    # for a in DeputatItem.select().where(DeputatItem.id < 11).order_by(DeputatItem.id):
        try:
            deputats_voting_dict = [{k:v for k, v in i.items() if k in 'Голосування депутата'} for i in ast.literal_eval(a.toptitle_info)][0]
            deputats_voting_url = next(iter(deputats_voting_dict.values()))
            kod_deputat = deputats_voting_url.split('kod=')[1]
            deputats_personal_voting(kod_deputat, a.id)
        except Exception as e:
            print(e)
            print('EX:',a.id)
            exeption_id_list.append(a.id)
        time.sleep(0.1)
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = f'Complete deputy id: {a.id}', length = 50)
    print(exeption_id_list)

    # Check loop
    # a = DeputatItem.get(DeputatItem.id == 3)
    # print(a.name)
    # print(a.toptitle_info)
    # print(a.url)
    # # VoteMeta_Django = VoteMeta_Django.alias()
    # # DeputatItem = DeputatItem.alias()
    # # GeneralVote_Django = GeneralVote_Django.alias()
    # print(ast.literal_eval(a.toptitle_info))
    # deputats_voting_dict = [{k:v for k, v in i.items() if k in 'Голосування депутата'} for i in ast.literal_eval(a.toptitle_info)][0]
    # print('+'*10, deputats_voting_dict)
    # deputats_voting_url = next(iter(deputats_voting_dict.values()))
    # kod_deputat = deputats_voting_url.split('kod=')[1]
    # deputats_personal_voting(kod_deputat, a.id)
