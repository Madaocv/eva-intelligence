import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import os
clear_migrations = 'find . -path "*/migrations/*.py" -not -name "__init__.py" -delete'
clear_migrations_1 = 'find . -path "*/migrations/*.pyc"  -delete'
try:
    # user = 'user_eva_01'
    # password = 'chGm8xfJ4JmQvxzL'
    # db_name = 'db_eva_02'
    # connection = psycopg2.connect(user = user,
    #                               password = password,
    #                               host = "127.0.0.1",
    #                               port = "5432",
    #                               database = db_name)
    os.system(clear_migrations)
    os.system(clear_migrations_1)
    connection = psycopg2.connect(database="postgres", user="eliiashiv", password="", host="127.0.0.1", port="5432")
    connection.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cursor = connection.cursor()
    cursor.execute('drop database if exists db_eva_02')
    cursor.execute('CREATE DATABASE db_eva_02')
    cursor.execute("CREATE USER user_eva_01 WITH PASSWORD 'chGm8xfJ4JmQvxzL'")
    cursor.execute("ALTER ROLE user_eva_01 SET client_encoding TO 'utf8'")
    cursor.execute("ALTER ROLE user_eva_01 SET default_transaction_isolation TO 'read committed'")
    cursor.execute("ALTER ROLE user_eva_01 SET timezone TO 'UTC'")
    cursor.execute("GRANT ALL PRIVILEGES ON DATABASE db_eva_02 to user_eva_01")
    # Print PostgreSQL Connection properties
    print ( connection.get_dsn_parameters(),"\n")
    # Print PostgreSQL version
    cursor.execute("SELECT version();")
    record = cursor.fetchone()
    print("You are connected to - ", record,"\n")
except (Exception, psycopg2.Error) as error :
    print ("Error while connecting to PostgreSQL", error)
finally:
    #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")