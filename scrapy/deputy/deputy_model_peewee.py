from peewee import *

user = 'user_eva_01'
password = 'chGm8xfJ4JmQvxzL'
db_name = 'db_eva_02'

pg_db = PostgresqlDatabase(
    db_name,
    user=user,
    password=password,
    host='localhost',
    port=5432,
    autorollback=True,
    )

class BaseModel(Model):
    class Meta:
        database = pg_db

class Deputy_Django(BaseModel):
    name = CharField(max_length=100, null=True)
    url = CharField(max_length=300, null=True)
    photo = TextField(null=True)
    photo_url = CharField(max_length=300, null=True)
    discriptional_info = TextField(null=True)
    general_info = TextField(null=True)
    work_hard = TextField(null=True)
    level_1_info = TextField(null=True)
    simple_info = TextField(null=True)
    toptitle_info = TextField(null=True)
    deputats_assistants = TextField(null=True)
    contacts_info = TextField(null=True)
    deputat_eregistr = TextField(null=True)
    deputat_manual_registr = TextField(null=True)
    deputat_frakcia = TextField(null=True)
    deputat_post = TextField(null=True)

    class Meta:
        db_table = "deputy_8"

class Deputy_9(BaseModel):
    name = CharField(max_length=100, null=True)
    url = CharField(max_length=300, null=True)
    photo = TextField(null=True)
    photo_url = CharField(max_length=300, null=True)
    discriptional_info = TextField(null=True)
    general_info = TextField(null=True)
    work_hard = TextField(null=True)
    level_1_info = TextField(null=True)
    simple_info = TextField(null=True)
    toptitle_info = TextField(null=True)
    deputats_assistants = TextField(null=True)
    contacts_info = TextField(null=True)
    deputat_eregistr = TextField(null=True)
    deputat_manual_registr = TextField(null=True)
    # deputat_frakcia = TextField(null=True)
    deputat_post = TextField(null=True)

    class Meta:
        db_table = "deputy_9"