from peewee import *
from deputy_model_peewee import BaseModel
from deputy_model_peewee import Deputy_9 as Deputy_Django
from law_model_peewee import Law_Django


class GeneralVote_Django(BaseModel):
    # Поіменне голосування  про проект Закону.... у другому читанні та в цілому
    name = CharField(
        max_length=10000,
        )
    # 25.12.2015 04:09:50
    timestamp = DateTimeField()
    # За-284
    count_yes = SmallIntegerField()
    # Проти-0
    count_not = SmallIntegerField()
    # Утрималось-2
    count_hold_out = SmallIntegerField()
    # Не голосувало-76
    count_not_vote = SmallIntegerField()
    # Всього- 385
    count_all = SmallIntegerField()
    # Рішення не прийняте, Рішення прийняте
    resolution = CharField(max_length=50)

    class Meta:
        db_table = "generalvote_9"

class Vote_Django(BaseModel):
    # http://w1.c1.rada.gov.ua/pls/radan_gs09/ns_golos?g_id=5447
    url = CharField(
        max_length=500,
    )
    law = ForeignKeyField(
        Law_Django,
        null=True,
    )
    # deputy = ForeignKeyField(
    #     Deputy_Django,
    #     null=True
    # )
    general = ForeignKeyField(
        GeneralVote_Django,
        primary_key=True,
        # null=True,
    )

    class Meta:
        db_table = "vote_9"


class VoteMeta_Django(BaseModel):
    # - за	•	- проти	•	- утримався	•	- не голосував	-	- відсутній
    deputy = ForeignKeyField(
        Deputy_Django,
        null=True
    )
    # law = ForeignKeyField(
    #     Law_Django,
    #     null=True
    # )
    vote = CharField(null=True, max_length=20)
    vote_change = CharField(null=True, max_length=1000)

    voteforeign = ForeignKeyField(
        Vote_Django,
        # on_delete=models.CASCADE,
        null=True,
        # primary_key=True,
        # blank=True
    )

    class Meta:
        db_table = "votemeta_9"