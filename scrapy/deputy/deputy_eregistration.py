# -*- encoding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import datetime
import base64
from urllib.parse import urljoin
import ast
from pprint import pprint
import argparse
from deputy_model_peewee import Deputy_9 as DeputatItem
from deputy_model_peewee import pg_db
from registration_model_peewee import Eregistration
import datetime
import pandas as pd
import time
from progress_bar_x import printProgressBar

def deputats_eregistr(kod):
    kod = kod
    url_eregistr ='http://w1.c1.rada.gov.ua'
    url_post_eregistr = 'http://w1.c1.rada.gov.ua/pls/radan_gs09/ns_dep_reg_list'
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
        }
    params = {
        "startDate": "29.08.2019",
        "endDate": datetime.datetime.now().date().strftime('%d.%m.%Y'),
        "kod": kod,
        "nom_str": "0"
        }
    resp = requests.post(url_post_eregistr, params=params, headers=headers)
    # print("response status code:", resp.status_code)
    soup = BeautifulSoup(resp.content, 'html.parser', from_encoding='utf-8')
    voting_li = soup.find("ul",{"class":"pd"}).find_all('li')

    v_global = []
    for idx, obj in enumerate(voting_li):
        try:
            vot = {}
            vot['exnomer'] = obj.find("div", {"class":"exnomer"}).text.strip()
            # '12.07.2019 10:11:56'
            vot['strdate'] = " ".join(obj.find("div", {"class":"strdate"}).text.strip().split())
            vot['zname'] = urljoin(url_eregistr, obj.find("div", {"class":"zname"}).a.get('href'))
            vot['name'] = obj.find("div", {"class":"zname"}).text.strip()
            vot['strvsego'] = obj.find("div", {"class":"strvsego"}).text.strip()
            vot['zrez'] = obj.find("div", {"class":"zrez"}).text.strip()
            vot['exception'] = None
            v_global.append(vot)
        except Exception:
            if obj.find("ul", {"class":"npd"}):
                obj2= obj.previous_sibling.previous_sibling
                vot = {}
                vot['exnomer'] = obj2.find("div", {"class":"exnomer"}).text.strip()
                vot['strdate'] = " ".join(obj2.find("div", {"class":"strdate"}).text.strip().split())
                vot['zname'] = urljoin(url_eregistr, obj2.find("div", {"class":"zname"}).a.get('href'))
                vot['name'] = obj.find("div", {"class":"zname"}).text.strip()
                vot['strvsego'] = obj2.find("div", {"class":"strvsego"}).text.strip()
                vot['zrez'] = obj2.find("div", {"class":"zrez"}).text.strip()
                vot['exception'] = ' '.join([a.strip() for a in obj.find("ul", {"class":"npd"}).text.strip().split('\n')])
                v_global.append(vot)
    return v_global

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Initialization of parsing scripts')
    parser.add_argument('--saveto', '-s', choices=['db', 'csv'], help='Save results to db/csv. Usage: script.py --deputy_eregistration csv') 
    args = vars(parser.parse_args())
    # Парсинг реєстрації депутатів за допомогою електронної системи відбувається шляхом переходу по url адресу http://w1.c1.rada.gov.ua/pls/radan_gs09/ns_dep_reg_list
    # Куда передається його індивідкальний код і часові інтервали вибірки: startDate, endDate.
    # Для депутатів 9 ого скликання параметр startDate є початок їх роботи - 29.08.2019, endDate - сьогоднішня дата
    if args['saveto']=='db':
        if 'eregistration_9' in pg_db.get_tables():
            pass
            # Eregistration.drop_table()
        else:
            Eregistration.create_table()
        l = len(DeputatItem.select().order_by(DeputatItem.id))
        for i, a in enumerate(DeputatItem.select().order_by(DeputatItem.id)):
            deputats_voting_dict = [{k:v for k, v in i.items() if k in 'Реєстрація депутата за допомогою електронної системи'} for i in ast.literal_eval(a.toptitle_info)][1]
            deputats_voting_url = next(iter(deputats_voting_dict.values()))
            kod_deputat = deputats_voting_url.split('kod=')[1]
            for idx, object_eregistration in enumerate(deputats_eregistr(kod_deputat)):
                timestamp = datetime.datetime.strptime(object_eregistration['strdate'], '%d.%m.%Y %H:%M:%S')
                query_eregistration = Eregistration.select().where((Eregistration.edeputy_id == a.id) & (Eregistration.eregistration_url == object_eregistration['zname']))
                if query_eregistration.exists():
                    pass
                else:
                    try:
                        Eregistration.create(
                            edeputy_id = a.id,
                            timestamp = timestamp,
                            eregistration_name = object_eregistration['name'],
                            eregistration = object_eregistration['zrez'],
                            eregistration_change = object_eregistration['exception'],
                            eregistration_all = object_eregistration['strvsego'],
                            eregistration_url = object_eregistration['zname'],
                        )
                    except Exception as e:
                        print(e)
            printProgressBar(i + 1, l, prefix = 'Progress:', suffix = f'Complete deputy id: {a.id}', length = 50)
    elif args['saveto']=='csv':
        data = pd.read_csv("export_dataframe_with_details.csv") 
        list_to_csv = []
        l = len(data['toptitle_info'])
        for i, a in enumerate(data['toptitle_info']):
            deputats_voting_dict = [{k:v for k, v in i.items() if k in 'Реєстрація депутата за допомогою електронної системи'} for i in ast.literal_eval(a)][1]
            deputats_voting_url = next(iter(deputats_voting_dict.values()))
            kod_deputat = deputats_voting_url.split('kod=')[1]
            list_to_csv.append(deputats_eregistr(kod_deputat))
            printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        data_new = pd.Series(list_to_csv, name='eregistration_9')
        data_frame = data.join(data_new)
        data_frame.to_csv('export_dataframe_with_details_eregistration.csv', index = None, header=True)
