from peewee import *
from deputy_model_peewee import BaseModel
from deputy_model_peewee import Deputy_9

class Eregistration(BaseModel):
    edeputy = ForeignKeyField(Deputy_9)
    # '12.07.2019 10:11:56'
    timestamp = DateTimeField()
    # Ранкова реєстрація, Вечірня реєстрація
    eregistration_name= CharField(max_length=50)
    # 'Зареєстрований' , 'У відрядженні', 'Незареєстрований'
    eregistration = CharField(max_length=50)
    # '* До Голови Верховної Ради України надійшла заява .... проханням вважати результатом її електронної реєстрації - "Відсутня".'
    eregistration_change = CharField(null=True, max_length=1000)

    # Всього- 385
    eregistration_all = SmallIntegerField()
    # 'http://w1.c1.rada.gov.ua/pls/radan_gs09/ns_reg?g_id=29069'
    eregistration_url = CharField(max_length=500)
    class Meta:
        db_table = "eregistration_9"

class Mregistration(BaseModel):
    mdeputy = ForeignKeyField(Deputy_9)
    # '12.07.2019'
    date = DateField()
    # Ранкове засідання, Вечірнє засідання
    mregistration_name= CharField(max_length=50)
    mregistration_url = CharField(max_length=500)
    # '7 сесія', '6 сесія'
    nomses = CharField(max_length=50)
    # 'Присутній' , 'Відсутній'
    mregistration = CharField(max_length=50)
    class Meta:
        db_table = "mregistration_9"