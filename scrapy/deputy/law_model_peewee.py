from peewee import *
from deputy_model_peewee import BaseModel

class Law_Django(BaseModel):
    name = CharField(null=True, max_length=10000)
    url = CharField(null=True, max_length=10000)
    date = DateTimeField(null=True)
    zn_attr = CharField(null=True, max_length=1000)
    detail_items = CharField(null=True, max_length=50000)
    flow_tab = CharField(null=True, max_length=10000)
    komited_tab = CharField(null=True, max_length=10000)
    linked_tab = CharField(null=True, max_length=1000)
    alternative_tab = CharField(null=True, max_length=1000)

    class Meta:
        db_table = "law"
        # ordering = ['id']