# -*- encoding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import base64
from deputy_model_peewee import Deputy_9 as DeputatItem
from deputy_model_peewee import pg_db
import argparse
import pandas as pd
import time
from progress_bar_x import printProgressBar

def deputy_list(saveto):
    # Цей параметр skl робить запит на депутатів, наприклад 10 - вибірка по депутатам 9ого скликання
    skl = "10"
    url = "http://w1.c1.rada.gov.ua/pls/site2/fetch_mps"
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
        }
    # params - для депутатів які не склали мандат, тобто діючі
    params = {
        "skl_id": skl,
        }
    # params_out_deputy - для депктатів які склали мандат
    params_out_deputy = {
        "skl_id": skl,
        "pid_id": "-3"
        }
    # Запит за допомогою requests, пасинг результату Response за допомогою BeautifulSoup
    resp = requests.get(url, params=params, headers=headers)
    print('Response Status Code:', resp.status_code)
    soup = BeautifulSoup(resp.content, 'lxml', from_encoding='utf-8')

    search_results = soup.find("ul", {"class": "search-filter-results"}).find_all('li')

    idx_f = 1
    list_to_csv = []
    l = len(search_results)
    for i, obj in enumerate(search_results):
        deputat = {}
        image_url = obj.img.get('src')
        deputat['image_url'] = image_url
        try:
            photo = base64.b64encode(requests.get(image_url, headers=headers).content)
        except Exception as e:
            photo = None
        deputat['photo'] = photo
        url = obj.a.get('href')
        deputat['url'] = url
        name = obj.a.text
        deputat['name'] = name

        ful_list_dd = []
        for idx, item in enumerate(obj.find_all('dd')):
            if len(item.text) > 0:
                ful_list_dd.append(item.text)
        ful_list_dt = []
        for idx, item in enumerate(obj.find_all('dt')):
            if len(item.text) > 0:
                ful_list_dt.append(item.text)
        tuplelist = list(zip(ful_list_dt, ful_list_dd))

        detail_items = {a: b for a, b in tuplelist}
        deputat['discriptional_info'] = detail_items
        # цей append добавляє словники з депутатами в список для подальшого збереження в csv файл 
        list_to_csv.append(deputat)
        # У випадку коли переданий аргумент --saveto db, тутже зберігаються дані в базу
        printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        if saveto == 'db':
            query = DeputatItem.select().where(DeputatItem.name == deputat['name'])

            if query.exists():
                pass
                # print('Row exist')
            else:
                try:
                    deputat_item = DeputatItem.create(
                        name=deputat['name'],
                        url=deputat['url'],
                        photo=deputat['photo'],
                        photo_url=deputat['image_url'],
                        discriptional_info=deputat['discriptional_info']
                        )
                    # print('Saved :', idx_f)
                    idx_f = idx_f + 1
                except Exception as e:
                    print('Exception', '-'*30, e)
    # У випадку коли переданий аргумент --saveto csv
    if saveto == 'csv':
        df = pd.DataFrame(list_to_csv)
        df.to_csv('export_dataframe.csv', index = None, header=True)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Initialization of parsing scripts')
    parser.add_argument('--saveto', '-s', choices=['db', 'csv'], help='Save results to db/csv. Usage: deputy_list.py --saveto csv') 
    args = vars(parser.parse_args())

    # В кінцевому результаті ми отримаємо 400+ записів в таблиці deputy_9 з полями: name, url, photo, photo_url, discriptional_info
    if args['saveto']=='db':
        # Check if table deputy_9 exist in database
        if 'deputy_9' in pg_db.get_tables():
            print('Table deputy_9 exist')
        else:
            # Create the tables.
            # DeputatItem.drop_table()
            DeputatItem.create_table()
        deputy_list('db')
    elif args['saveto']=='csv':
        deputy_list('csv')

