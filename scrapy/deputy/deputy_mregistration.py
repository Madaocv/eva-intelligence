# -*- encoding: utf-8 -*-
from bs4 import BeautifulSoup
import requests
import datetime
import base64
from urllib.parse import urljoin
import ast
from pprint import pprint
import argparse
from deputy_model_peewee import Deputy_9 as DeputatItem
from deputy_model_peewee import pg_db
from registration_model_peewee import Mregistration
import datetime
import pandas as pd
import time
from progress_bar_x import printProgressBar

def deputats_manual_registr(kod):
    kod = kod
    url_eregistr ='http://w1.c1.rada.gov.ua'
    url_post_eregistr = 'http://w1.c1.rada.gov.ua/pls/radan_gs09/ns_dep_reg_w_list'
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36"
        }
    params = {
        "startDate": "29.08.2019",
        "endDate": datetime.datetime.now().date().strftime('%d.%m.%Y'),
        "kod": kod,
        "nom_str": "0"
        }
    resp = requests.post(url_post_eregistr, params=params, headers=headers)
    # print("response status code:", resp.status_code)
    soup = BeautifulSoup(resp.content, 'html.parser', from_encoding='utf-8')
    voting_li = soup.find("ul",{"class":"pd"}).find_all('li')
    v_global = []
    for idx, obj in enumerate(voting_li):
        vot = {}
        vot['exnomer'] = obj.find("div", {"class":"exnomer"}).text.strip()
        vot['nomses'] = obj.find("div", {"class":"nomses"}).text.strip()
        vot['strdate'] = obj.find("div", {"class":"strdate"}).b.text.strip()
        vot['zname'] = urljoin(url_eregistr, obj.find("div", {"class":"zname"}).a.get('href'))
        vot['zname_text'] = obj.find("div", {"class":"zname"}).a.text.strip()
        vot['zrez'] = obj.find("div", {"class":"zrez"}).text.strip()
        v_global.append(vot)
    return v_global

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Initialization of parsing scripts')
    parser.add_argument('--saveto', '-s', choices=['db', 'csv'], help='Save results to db/csv. Usage: script.py --saveto csv') 
    args = vars(parser.parse_args())
    if args['saveto']=='db':
        if 'mregistration_9' in pg_db.get_tables():
            pass
            # Mregistration.drop_table()
        else:
            Mregistration.create_table()
        # for a in DeputatItem.select().where(DeputatItem.id < 3).order_by(DeputatItem.id):
        l = len(DeputatItem.select().order_by(DeputatItem.id))
        for i, a in enumerate(DeputatItem.select().order_by(DeputatItem.id)):
            deputats_voting_dict = [{k:v for k, v in i.items() if k in 'Письмова реєстрація депутата'} for i in ast.literal_eval(a.toptitle_info)][2]
            deputats_voting_url = next(iter(deputats_voting_dict.values()))
            kod_deputat = deputats_voting_url.split('kod=')[1]
            for idx, object_eregistration in enumerate(deputats_manual_registr(kod_deputat)):
                timestamp = datetime.datetime.strptime(object_eregistration['strdate'], '%d.%m.%Y').date()
                query_eregistration = Mregistration.select().where((Mregistration.mdeputy_id == a.id) & (Mregistration.mregistration_url == object_eregistration['zname']))
                if query_eregistration.exists():
                    pass
                else:
                    try:
                        Mregistration.create(
                            mdeputy_id = a.id,
                            date = timestamp,
                            mregistration_name = object_eregistration['zname_text'],
                            mregistration = object_eregistration['zrez'],
                            nomses = object_eregistration['nomses'],
                            mregistration_url = object_eregistration['zname'],
                        )
                    except Exception as e:
                        print(e)
            printProgressBar(i + 1, l, prefix = 'Progress:', suffix = f'Complete deputy id: {a.id}', length = 50)
    elif args['saveto']=='csv':
        data = pd.read_csv("export_dataframe_with_details.csv") 
        list_to_csv = []
        l = len(data['toptitle_info'])
        for i, a in enumerate(data['toptitle_info']):
            deputats_voting_dict = [{k:v for k, v in i.items() if k in 'Письмова реєстрація депутата'} for i in ast.literal_eval(a)][2]
            deputats_voting_url = next(iter(deputats_voting_dict.values()))
            kod_deputat = deputats_voting_url.split('kod=')[1]
            list_to_csv.append(deputats_manual_registr(kod_deputat))
            printProgressBar(i + 1, l, prefix = 'Progress:', suffix = 'Complete', length = 50)
        data_new = pd.Series(list_to_csv, name='mregistration_9')
        data_frame = data.join(data_new)
        data_frame.to_csv('export_dataframe_with_details_mregistration.csv', index = None, header=True)