# Usage: python manage.py runscript check_eregistration_single
from pprint import pprint
from django.db.models import Q
from eregistration.models import Eregistration9
from deputy.models import Deputy9
def run():
    # https://itd.rada.gov.ua/mps/info/page/20966
    # Аллахвердієва Ірина Валеріївна
    deputy_5 = Deputy9.objects.get(id=5)
    # deputy_1_eregistration = deputy_1.edeputy_id.all()
    deputy_5_pluss = deputy_5.edeputy_id.filter(eregistration__contains="Присутн").count()
    deputy_5_minus = deputy_5.edeputy_id.filter(~Q(eregistration__contains="Присутн")).count()
    print('+', deputy_5_pluss)
    print('-', deputy_5_minus)
