from django.db import models
from deputy.models import Deputy8, Deputy, Deputy9
from law.models import Law, Law9
# Create your models here.
class GeneralVote(models.Model):
    # Поіменне голосування  про проект Закону.... у другому читанні та в цілому
    name = models.CharField(
        max_length=10000,
        )
    # 25.12.2015 04:09:50
    timestamp = models.DateTimeField(
        auto_now=False,
        auto_now_add=False,
    )
    # За-284
    count_yes = models.PositiveSmallIntegerField(
        null=True,
        blank=True
    )
    # Проти-0
    count_not = models.PositiveSmallIntegerField(
        null=True,
        blank=True
    )
    # Утрималось-2
    count_hold_out = models.PositiveSmallIntegerField(
        null=True,
        blank=True
    )
    # Не голосувало-76
    count_not_vote = models.PositiveSmallIntegerField(
        null=True,
        blank=True
    )
    # Всього- 385
    count_all = models.PositiveSmallIntegerField(
        null=True,
        blank=True
    )
    # Рішення не прийняте, Рішення прийняте
    resolution = models.CharField(
        max_length=50,
        null=True,
        blank=True
        )

    def __str__(self):
        return "ID: %s, %s..." % (self.id, self.name[:150])

    class Meta:
        abstract = True
        db_table = "generalvote"
        ordering = ['id']


class Vote(models.Model):
    # http://w1.c1.rada.gov.ua/pls/radan_gs09/ns_golos?g_id=5447
    url = models.URLField(
        max_length=500,
    )
    law = models.ForeignKey(
        Law,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    general = models.OneToOneField(
        GeneralVote,
        on_delete=models.CASCADE,
        primary_key=True,
        # null=True,
        # blank=True
    )
    def __str__(self):
        return "General ID: %s " % (self.general_id)
    class Meta:
        abstract = True
        db_table = "vote"
        ordering = ['general_id']


class VoteMeta(models.Model):
    # - за	•	- проти	•	- утримався	•	- не голосував	-	- відсутній
    deputy = models.ForeignKey(
        Deputy,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    vote = models.CharField(null=True, blank=True, max_length=20)
    vote_change = models.CharField(null=True,blank=True, max_length=1000)
    voteforeign = models.ForeignKey(
        Vote,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    def __str__(self):
        return "Проголосував: %s, Змінив рішення ? :%s" % (self.vote, self.vote_change)
    class Meta:
        abstract = True
        db_table = "votemeta"
        # ordering = ['id']

class GeneralVote9(GeneralVote):
    class Meta:
        db_table = "generalvote_9"
class Vote9(Vote):
    law = models.ForeignKey(
        Law9,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    general = models.OneToOneField(
        GeneralVote9,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    class Meta:
        db_table = "vote_9"
class VoteMeta9(VoteMeta):
    deputy = models.ForeignKey(
        Deputy9,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    voteforeign = models.ForeignKey(
        Vote9,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    class Meta:
        db_table = "votemeta_9"