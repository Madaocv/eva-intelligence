from django.contrib import admin

# Register your models here.
from vote.models import Vote9
from vote.models import VoteMeta9
from vote.models import GeneralVote9
admin.site.register(Vote9)
admin.site.register(VoteMeta9)
admin.site.register(GeneralVote9)
